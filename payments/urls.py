from django.urls import path, include #add include
from . import views

urlpatterns = [
    path("", views.homepage, name='home'),
    path('checkout/', views.checkout, name="checkout"),
    path("create-sub", views.create_sub, name="create sub"),
	path("complete", views.complete, name="complete"),
]